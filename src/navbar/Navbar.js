import './index.scss';
import dashboardLogo from './dashboardLogo.svg';
import polygon from '../sharedAssets/polygon.svg';

function Navbar({ isDirty, onDiscard, onSave }) {
  return (
    <div className="navbar">
      <div className="dashboard-logo-box noselect">
        <img src={ dashboardLogo } className="dashboard-logo" />
      </div>
      <div className="dashboard-logo-tooltip">
        <img src={ polygon } className="dashboard-logo-tooltip-arrow" />
        <div className="dashboard-logo-tooltip-box">
          Open Dashboard
        </div>
      </div>
      <div className="navbar-title">
        <span className="navbar-title-library">Material UI</span>
        <span className="navbar-title-divider">&nbsp;/&nbsp;</span>
        <span className="navbar-title-component">Button</span>
      </div>
      <div className="navbar-filler"></div>
      <div 
        className="navbar-discard-changes light-select"
        onClick={ onDiscard }
      >
        Discard changes
      </div>
      <div 
        className={ 
          `navbar-save-changes ${
            isDirty ? 
              "navbar-save-changes-button-active noselect" : 
              "navbar-save-changes-button-inactive light-select"
          }`
        }
        onClick={ onSave }
      >
        { isDirty ? "Save changes" : "No changes to save" }
      </div>
    </div>
  )
}

export default Navbar;