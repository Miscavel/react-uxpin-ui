import "./index.scss";
import closeIcon from "./close.svg";
import openIcon from "./open.svg";

function ToggleIcon({ open = false, onClick }) {
  function handleOnClick(e) {
    e.stopPropagation();
    onClick?.();
  }

  return (
    <div className="toggle-icon-box noselect" onClick={ handleOnClick }>
      <img src={ open ? closeIcon : openIcon } className="toggle-icon" />
    </div>
  );
}

export default ToggleIcon;