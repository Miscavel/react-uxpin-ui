import './index.scss';
import eyeCon from './eyecon.svg';
import eyeConHidden from './eyecon-hidden.svg';

function EyeCon({ active = true, onToggle }) {
  function handleEyeconToggle(e) {
    e.stopPropagation();
    onToggle?.();
  }

  return (
    <div 
      className="eyecon-box noselect"
      onClick={ handleEyeconToggle }
    >
      <img src={ active ? eyeCon : eyeConHidden } className="eyecon" />
    </div>
  );
}

export default EyeCon;