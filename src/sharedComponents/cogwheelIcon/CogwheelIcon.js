import './index.scss';
import gear from './gear.svg'

function CogwheelIcon() {
  return (
    <div className="cogwheel-icon-box noselect">
      <img src={ gear } className="cogwheel-icon" />
    </div>
  )
}

export default CogwheelIcon;