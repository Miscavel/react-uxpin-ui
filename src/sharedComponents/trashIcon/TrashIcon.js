import "./index.scss";
import trash from "./trash.svg";

function TrashIcon({ onClick }) {
  function handleOnClick() {
    onClick?.();
  }
  return (
    <div className="trash-icon-box noselect" onClick={ handleOnClick }>
      <img src={ trash } className="trash-icon" />
    </div>
  );
}

export default TrashIcon;