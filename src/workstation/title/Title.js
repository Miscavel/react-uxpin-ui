import './index.scss';
import EyeCon from '../../sharedComponents/eyeCon/EyeCon';
import CogwheelIcon from '../../sharedComponents/cogwheelIcon/CogwheelIcon';
import { useState } from 'react';
import polygon from '../../sharedAssets/polygon.svg';

function Title() {
  const [ isActive, setActive ] = useState(true);

  function handleEyeconToggle() {
    setActive(!isActive);
  }

  return (
    <div className="title">
      <div className="title-text">
        Button
      </div>
      <EyeCon active={ isActive } onToggle={ handleEyeconToggle }></EyeCon>
      <div className="eyecon-tooltip">
        <img src={ polygon } className="eyecon-tooltip-arrow" />
        <div className="eyecon-tooltip-box">
          Toggle component visibility in library
        </div>
      </div>
      <CogwheelIcon></CogwheelIcon>
      <div className="cogwheel-icon-tooltip">
        <img src={ polygon } className="cogwheel-icon-tooltip-arrow" />
        <div className="cogwheel-icon-tooltip-box">
          Component settings
        </div>
      </div>
    </div>
  )
}

export default Title;