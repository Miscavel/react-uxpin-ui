import './index.scss';
import Button from '@mui/material/Button';
import ErrorBoundary from './ErrorBoundary';

function ComponentPreview({ properties }) {
  function getOpts(prop) {
    const opts = {}
    prop.forEach(function (property) {
      let value;
      switch(property.propertyControl) {
        case "select": {
          value = property.selectDefaultValue;
          break;
        }

        case "textarea": {
          value = property.textAreaDefaultValue;
          break;
        }

        default: {
          value = (property.booleanDefaultValue === "true") ? true : false;
          break;
        }
      }
      opts[property.name] = value;
    });
    return opts;
  }

  function getSignature(prop) {
    return prop[0]?.signature || -1;
  }

  const opts = getOpts(properties);
  const signature = getSignature(properties);

  return (
    <div className="component-preview">
      <div className="component-preview-title">Component Preview</div>
      <div className="component-preview-content">
        <ErrorBoundary key={ signature } errorText={ 'Invalid button styling' }>
          <Button { ...opts } ></Button>
        </ErrorBoundary>
      </div>
    </div>
  )
}

export default ComponentPreview;