import './index.scss';
import PropertyForm from './propertyForm/PropertyForm';
import PropertyEditor from './propertyEditor/PropertyEditor';
import { useState } from 'react';

function Properties({ _properties, onPropertiesChange, setNewProperties }) {
  const [ properties, setProperties ] = useState(_properties);
  setNewProperties.dispatcher = setProperties;

  function handleOnAddProperty(data) {
    const id = properties[properties.length - 1].id + 1;
    const newProperties = [...properties];
    newProperties.push({ ...data, id });
    setProperties(newProperties);
    onPropertiesChange?.(newProperties);
  }

  function handleOnDeleteProperty(id) {
    const newProperties = [...properties].filter(function (property) {
      return property.id != id;
    });
    setProperties(newProperties);
    onPropertiesChange?.(newProperties);
  }

  function compareProperties(newProperty, oldProperty) {
    let isDirty = false;
    Object.entries(newProperty).forEach(function ([key, val]) {
      if (oldProperty[key] !== val) {
        isDirty = true;
      }
    });
    return isDirty;
  }

  function handleOnChangeProperty(data) {
    const oldProperty = properties.find(function (property) {
      return property.id === data.id;
    });
    const isDirty = compareProperties(
      data,
      oldProperty
    );
    oldProperty.isDirty = isDirty;
    oldProperty.overwrite = data;
    onPropertiesChange?.(properties);
  }

  return (
    <div className="properties">
      <PropertyForm
        onAddProperty={ handleOnAddProperty }
      >
      </PropertyForm>
      {
        properties.map(function (property) {
          return (
            <PropertyEditor 
              propertyData={ property } 
              key={ property.id } 
              onDeleteProperty={ handleOnDeleteProperty }
              onChangeProperty={ handleOnChangeProperty }
            ></PropertyEditor>
          );
        })
      }
    </div>
  )
}

export default Properties;