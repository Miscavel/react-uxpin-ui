import './index.scss';
import PropertyField from './propertyField/PropertyField';
import { 
  PROPERTY_TYPES, 
  PROPERTY_TYPE_DEFAULT_VALUE, 
  PROPERTY_CONTROL, 
  PROPERTY_CONTROL_DEFAULT_VALUE,
  PROPERTY_CONTROL_INCLUDE_OPTIONS,
  PROPERTY_TEXT_AREA_DEFAULT_VALUE_DEFAULT_ROW
} from './constants';
import { useEffect, useState } from 'react';

function PropertyDetail(prop) {
  const {
    signature, 
    _name = "",
    _displayName = "",
    _description = "",
    _propertyType = PROPERTY_TYPE_DEFAULT_VALUE,
    _propertyControl = PROPERTY_CONTROL_DEFAULT_VALUE,
    _textAreaDefaultValueRow = PROPERTY_TEXT_AREA_DEFAULT_VALUE_DEFAULT_ROW,
    _textAreaDefaultValue = "",
    _selectOptionsStr = "",
    _selectDefaultValue = "",
    _booleanDefaultValue = "true",
    isOpen = true, 
    visible = true,
    onStateUpdate 
  } = prop;
  const [ currentSignature, setCurrentSignature ] = useState(signature);
  const [ name, setName ] = useState(_name);
  const [ displayName, setDisplayName ] = useState(_displayName);
  const [ description, setDescription ] = useState(_description);
  const [ propertyType, setPropertyType ] = useState(_propertyType);
  const [ propertyControl, setPropertyControl ] = useState(_propertyControl);
  const [ textAreaDefaultValueRow, setTextAreaDefaultValueRow ] = useState(_textAreaDefaultValueRow);
  const [ textAreaDefaultValue, setTextAreaDefaultValue ] = useState(_textAreaDefaultValue);
  const [ selectOptionsStr, setSelectOptionsStr ] = useState(_selectOptionsStr);
  const [ selectOptions, setSelectOptions ] = useState(parseOptions(_selectOptionsStr));
  const [ selectDefaultValue, setSelectDefaultValue ] = useState(_selectDefaultValue);
  const [ booleanDefaultValue, setBooleanDefaultValue ] = useState(_booleanDefaultValue);

  function handleNameChange(e) {
    setName(e.target.value);
  }

  function handleDisplayNameChange(e) {
    setDisplayName(e.target.value);
  }
  
  function handleDescriptionChange(e) {
    setDescription(e.target.value);
  }

  function handlePropertyTypeChange(e) {
    const type = e.target.value;
    setPropertyType(type);
    setPropertyControl(PROPERTY_CONTROL[type]?.[0] || "");
  }

  function handlePropertyControlChange(e) {
    setPropertyControl(e.target.value);
  }

  function handleTextAreaDefaultValueRowChange(e) {
    setTextAreaDefaultValueRow(e.target.value);
  }

  function handleTextAreaDefaultValueChange(e) {
    setTextAreaDefaultValue(e.target.value);
  }

  function parseOptions(optionsStr) {
    return optionsStr.split(',').map(function (str) {
      return str.trim();
    });
  }

  function handleSelectOptionsStrChange(e) {
    const { value } = e.target;
    setSelectOptionsStr(value);

    const options = parseOptions(value);
    setSelectOptions(options);
    setSelectDefaultValue(options?.[(options?.length || 0) - 1] || "");
  }

  function handleSelectDefaultValueChange(e) {
    setSelectDefaultValue(e.target.value);
  }

  function handleBooleanDefaultValueChange(value) {
    setBooleanDefaultValue(value);
  }

  useEffect(function () {
    onStateUpdate?.(
      {
        name,
        displayName,
        description,
        propertyType,
        propertyControl,
        textAreaDefaultValueRow,
        textAreaDefaultValue,
        selectOptionsStr,
        selectDefaultValue,
        booleanDefaultValue, 
        visible,
      }
    );
  });

  useEffect(function () {
    const {
      signature,
      _name = "",
      _displayName = "",
      _description = "",
      _propertyType = PROPERTY_TYPE_DEFAULT_VALUE,
      _propertyControl = PROPERTY_CONTROL_DEFAULT_VALUE,
      _textAreaDefaultValueRow = PROPERTY_TEXT_AREA_DEFAULT_VALUE_DEFAULT_ROW,
      _textAreaDefaultValue = "",
      _selectOptionsStr = "",
      _selectDefaultValue = "",
      _booleanDefaultValue = "true",
    } = prop;
    if (signature !== currentSignature) {
      setCurrentSignature(signature);
      setName(_name);
      setDisplayName(_displayName);
      setDescription(_description);
      setPropertyType(_propertyType);
      setPropertyControl(_propertyControl);
      setTextAreaDefaultValueRow(_textAreaDefaultValueRow);
      setTextAreaDefaultValue(_textAreaDefaultValue);
      setSelectOptionsStr(_selectOptionsStr);
      setSelectDefaultValue(_selectDefaultValue);
      setBooleanDefaultValue(_booleanDefaultValue);
    }
  }, [prop]);

  return (
    <div className={ `property-detail ${visible ? "" : "invisible"}` }>
      {
        isOpen &&
        <>
          <PropertyField
            name="Property name"
            type="text"
            description={ 
              [
                { text: "(name of the property given in code)" }
              ]
            }
            value={ name }
            onChange={ handleNameChange }
          >
          </PropertyField>
          <PropertyField
            name="Display name"
            type="text"
            value= { displayName }
            onChange={ handleDisplayNameChange }
          >
          </PropertyField>
          <PropertyField
            name="Description"
            type="textarea"
            options={{ rows: "3" }}
            value={ description }
            onChange={ handleDescriptionChange }
          >
          </PropertyField>
          <PropertyField
            name="Property type"
            type="select"
            options={
              {
                selectValues: PROPERTY_TYPES
              }
            }
            onChange={ handlePropertyTypeChange }
            value={ propertyType }
          >
          </PropertyField>
          {
            PROPERTY_CONTROL[propertyType] &&
            <PropertyField
              name="Property control"
              type="select"
              options={
                { 
                  extraControls: {
                    "textarea": [
                      { 
                        type: "number", 
                        placeholder: "rows", 
                        min: "1", 
                        max: "10",
                        onChange: handleTextAreaDefaultValueRowChange,
                        value: textAreaDefaultValueRow,
                      }
                    ]
                  }[propertyControl],
                  selectValues: PROPERTY_CONTROL[propertyType] 
                }
              }
              description={
                [
                  { text: "(type of control displayed in editor's properties panel. ", type: "regular" },
                  { text: "Learn more", type: "link" },
                  { text: " about control types)", type: "regular" }
                ]
              }
              onChange= { handlePropertyControlChange }
              value={ propertyControl }
            >
            </PropertyField>
          }
          {
            PROPERTY_CONTROL_INCLUDE_OPTIONS.includes(propertyControl) &&
            <PropertyField
              name="Options"
              type="textarea"
              options={{ rows: "2" }}
              description={
                [
                  { text: "(list of options separated by comma)" }
                ]
              }
              onChange={ handleSelectOptionsStrChange }
              value={ selectOptionsStr }
            >
            </PropertyField>
          }
          {
            {
              "select": <PropertyField
                          name="Default value"
                          type="select"
                          options={
                            {
                              selectValues: selectOptions
                            }
                          }
                          value={ selectDefaultValue }
                          onChange={ handleSelectDefaultValueChange }
                        >   
                        </PropertyField>,
              "textarea": <PropertyField
                            name="Default value"
                            type="textarea"
                            options={{ rows: `${textAreaDefaultValueRow}` }}
                            value={ textAreaDefaultValue }
                            onChange={ handleTextAreaDefaultValueChange }
                          >
                          </PropertyField>
            }[propertyControl] ||
            {
              "boolean":  <PropertyField
                            name="Default value"
                            type="boolean"
                            value={ booleanDefaultValue }
                            onChange={ handleBooleanDefaultValueChange }
                          >
                          </PropertyField>
            }[propertyType]
          }
        </>  
      }
    </div>
  )
}

export default PropertyDetail;