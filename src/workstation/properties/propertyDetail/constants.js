export const PROPERTY_TYPES = [
  "one of",
  "node",
  "boolean"
];

export const PROPERTY_TYPE_DEFAULT_VALUE = PROPERTY_TYPES[0];

export const PROPERTY_CONTROL = {
  "one of": [
    "select"
  ],
  "node": [
    "textarea"
  ],
};

export const PROPERTY_CONTROL_DEFAULT_VALUE = PROPERTY_CONTROL[PROPERTY_TYPE_DEFAULT_VALUE]?.[0] || "";

export const PROPERTY_CONTROL_INCLUDE_OPTIONS = [ "select" ];

export const PROPERTY_TEXT_AREA_DEFAULT_VALUE_DEFAULT_ROW = 2;