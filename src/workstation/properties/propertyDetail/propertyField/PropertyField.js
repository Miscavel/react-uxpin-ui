import './index.scss';

function PropertyField({ name, value, type, description, options = {}, onChange }) {
  function handleBooleanOptionClick(e) {
    const value = e.target.getAttribute("data-value");
    onChange(value);
  }

  return (
    <div className="property-field">
      <label className="property-field-label">{ name }</label>
      {
        {
          "text": <input 
                    type="text" 
                    className="property-field-input-text" 
                    value={ value }
                    onChange={ onChange }
                  />,
          "textarea": <textarea 
                        className="property-field-textarea" 
                        rows={ options.rows }
                        onChange={ onChange }
                        value={ value }
                      >
                      </textarea>,
          "select": <select className="property-field-select" onChange={ onChange } value={ value }>
                      {
                        options?.selectValues?.map(function (value, index) {
                          return <option key={ index } value={ value }>{ value }</option>
                        })
                      }
                    </select>,
          "boolean":  <div className="property-field-boolean" onClick={ handleBooleanOptionClick }>
                        <div 
                          className={ `property-field-boolean-option left ${ value === "true" ? "active" : "" }` } 
                          data-value="true"
                        >
                          True
                        </div>
                        <div 
                          className={ `property-field-boolean-option ${ value === "true" ? "" : "active" }` }
                          data-value="false"
                        >
                          False
                        </div>
                      </div>
        }[type]
      }
      {
        options.extraControls?.map(function ({ type, placeholder, min, max, value, onChange }, index) {
          return (
            <input 
              type={ type } 
              className="property-field-extra-control" 
              placeholder={ placeholder }
              min={ min }
              max={ max }
              key={ index }
              defaultValue={ value }
              onChange={ onChange }
            />
          )
        })
      }
      {
        description &&
        <div className="property-field-description">
          { 
            description.map(function ({ type, text }, index) {
              return (
                <span
                  className={ 
                    `property-field-description-span-${type} ${type === "link" ? "light-select" : ""}`
                  } 
                  key={ index }
                >
                  { text }
                </span>
              )
            })
          }
        </div>
      }
    </div>
  )
}

export default PropertyField;