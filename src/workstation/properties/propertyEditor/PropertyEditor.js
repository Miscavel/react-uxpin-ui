import "./index.scss";
import PropertyDetail from '../propertyDetail/PropertyDetail';
import EyeCon from '../../../sharedComponents/eyeCon/EyeCon';
import TrashIcon from '../../../sharedComponents/trashIcon/TrashIcon';
import ToggleIcon from '../../../sharedComponents/toggleIcon/ToggleIcon';
import { useEffect, useState } from "react";
import polygon from '../../../sharedAssets/polygon.svg';

function PropertyEditor(prop) {
  const { propertyData, onDeleteProperty, onChangeProperty } = prop;
  const [ currentSignature, setCurrentSignature ] = useState(propertyData.signature);
  const [ displayName, setDisplayName ] = useState(propertyData.displayName);
  const [ isOpen, setIsOpen ] = useState(false);
  const [ isVisible, setIsVisible ] = useState(propertyData.visible);

  function handleOpenToggle() {
    setIsOpen(!isOpen);
  }

  function handleVisibilityToggle() {
    setIsVisible(!isVisible);
  }

  function handleOnDelete() {
    onDeleteProperty?.(propertyData.id);
  }

  function handleOnChange(data) {
    onChangeProperty?.({ ...data, id: propertyData.id });
  }

  useEffect(function () {
    const {
      signature,
      displayName,
      visible
    } = prop.propertyData;
    if (signature !== currentSignature) {
      setDisplayName(displayName);
      setCurrentSignature(signature);
      setIsVisible(visible);
    }
  }, [prop]);

  return (
  <div className={ `property-editor ${ isOpen ? "" : "closed"}` } onClick={ function() { setIsOpen(true); } }>
      <div className="property-editor-property-field">
        <div className={ `property-editor-property-name ${isVisible ? "" : "invisible"}` }>{ displayName }</div>
        <EyeCon active={ isVisible } onToggle={ handleVisibilityToggle }></EyeCon>
        <div className="property-editor-eyecon-tooltip">
          <img src={ polygon } className="property-editor-eyecon-tooltip-arrow" />
          <div className="property-editor-eyecon-tooltip-box">
            Hide property
          </div>
        </div>
        {
          isOpen &&
          <>
            <TrashIcon onClick={ handleOnDelete }></TrashIcon>
            <div className="property-editor-trash-icon-tooltip">
              <img src={ polygon } className="property-editor-trash-icon-tooltip-arrow" />
              <div className="property-editor-trash-icon-tooltip-box">
                Delete property
              </div>
            </div>
          </>
        }
      </div>
      <PropertyDetail
        signature={ propertyData.signature }
        id={ propertyData.id } 
        isOpen={ isOpen } 
        visible={ isVisible }
        _name={ propertyData.name }
        _displayName={ propertyData.displayName }
        _description={ propertyData.description }
        _propertyType={ propertyData.propertyType }
        _propertyControl={ propertyData.propertyControl }
        _textAreaDefaultValueRow={ propertyData.textAreaDefaultValueRow }
        _textAreaDefaultValue={ propertyData.textAreaDefaultValue }
        _selectOptionsStr={ propertyData.selectOptionsStr }
        _selectDefaultValue={ propertyData.selectDefaultValue }
        _booleanDefaultValue={ propertyData.booleanDefaultValue }
        onStateUpdate={ handleOnChange }
      ></PropertyDetail>
      <div className="property-editor-close-field">
        <div className="property-editor-filler"></div>
        <ToggleIcon open={ isOpen } onClick={ handleOpenToggle }></ToggleIcon>
      </div>
    </div>
  )
}

export default PropertyEditor;