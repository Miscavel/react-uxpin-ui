import './index.scss';
import PropertyDetail from '../propertyDetail/PropertyDetail';
import { useState } from 'react';

function PropertyForm({ onAddProperty }) {
  const [ isShowing, setIsShowing ] = useState(false);
  let newProperty = null;

  function handleOnAdd() {
    setIsShowing(false);
    onAddProperty?.(newProperty);
  }

  function handleStateUpdate(data) {
    newProperty = data;
  }

  return (
    <div className={ `properties-form ${isShowing ? "showing" : ""}` }>
      <div className="properties-title">
        <span className="properties-title-text">Properties</span>
        <span 
          className="add-properties"
          onClick={ function() { setIsShowing(true); } }
        >
          <span className="add-properties-plus noselect">+&nbsp;</span>
          <span className="add-properties-text light-select">Add new property</span>
        </span>
      </div>
      {
        isShowing &&
        <>
          <PropertyDetail
            onStateUpdate={ handleStateUpdate }
          >
          </PropertyDetail>
          <div className="properties-form-button-group">
            <div className="properties-form-button-group-filler"></div>
            <div 
              className="properties-form-cancel-link light-select"
              onClick={ function() { setIsShowing(false); } }
            >
              Cancel
            </div>
            <div 
              className="properties-form-add-button noselect"
              onClick={ handleOnAdd }
            >
              Add
            </div>
          </div>
        </>
      }
    </div>
  )
}

export default PropertyForm;