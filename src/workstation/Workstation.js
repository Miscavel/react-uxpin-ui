import './index.scss';
import Title from './title/Title';
import ComponentPreview from './componentPreview/ComponentPreview';
import Properties from './properties/Properties';

function Workstation({ properties, onPropertiesChange, setNewProperties }) {
  return (
    <div className="workstation">
      <Title></Title>
      <ComponentPreview properties={ properties }></ComponentPreview>
      <Properties 
        _properties={ properties }
        onPropertiesChange={ onPropertiesChange }
        setNewProperties={ setNewProperties }
      ></Properties>
    </div>
  )
}

export default Workstation;