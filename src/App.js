import './App.css';
import Navbar from './navbar/Navbar';
import Workstation from './workstation/Workstation';
import { DUMMY_PROPERTIES } from './data/dummy-properties';
import { useState } from 'react';

let properties = DUMMY_PROPERTIES;
let modifiedProperties;

function App() {
  let setNewProperties = { dispatcher: null };
  const [ isDirty, setIsDirty ] = useState(false);

  function copyProperties(prop) {
    let newProperties = [];
    prop.forEach(function (object) {
      newProperties.push({ ...object, signature: Date.now() });
    });
    return newProperties;
  }

  function compareProperties(newProperties, oldProperties) {
    if (newProperties.length !== oldProperties.length) return true;

    let isDirty = false;
    newProperties.forEach(function (property, index) {
      isDirty = isDirty || property.isDirty || property.id !== oldProperties[index].id;
    });
    return isDirty;
  }
  
  function handlePropertiesChange(newProperties) {
    const isDirty = compareProperties(newProperties, properties);
    setIsDirty(isDirty);
    modifiedProperties = newProperties;
  }

  function handleOnDiscard() {
    setNewProperties.dispatcher?.(copyProperties(properties));
  }

  function handleOnSave() {
    const newProperties = modifiedProperties || properties;
    newProperties.forEach(function (property, index, arr) {
      arr[index] = { ...property.overwrite };
    });
    properties = newProperties;
    setNewProperties.dispatcher?.(copyProperties(properties));
  }

  return (
    <div className="App">
      <Navbar isDirty={ isDirty } onDiscard={ handleOnDiscard } onSave={ handleOnSave }></Navbar>
      <Workstation 
        properties={ copyProperties(properties) }
        onPropertiesChange={ handlePropertiesChange }
        setNewProperties={ setNewProperties }
      >
      </Workstation>
    </div>
  );
}

export default App;
